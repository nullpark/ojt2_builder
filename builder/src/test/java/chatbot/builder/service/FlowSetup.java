package chatbot.builder.service;

import chatbot.builder.domain.dto.FlowRequestDto;
import chatbot.builder.domain.dto.StepDto;
import chatbot.builder.domain.entity.Flow;
import chatbot.builder.domain.entity.MessageStep;
import chatbot.builder.domain.entity.Step;
import chatbot.builder.domain.entity.WebsiteStep;
import chatbot.builder.repository.FlowRepository;
import chatbot.builder.repository.MessageStepRepository;
import chatbot.builder.repository.WebsiteStepRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

import static chatbot.builder.domain.dto.StepType.message;
import static chatbot.builder.domain.dto.StepType.website;

@Component
public class FlowSetup {

    @Autowired
    FlowRepository flowRepository;
    @Autowired
    MessageStepRepository messageStepRepository;
    @Autowired
    WebsiteStepRepository websiteStepRepository;

    public Flow makeFlow(){
        MessageStep messageStep1 = MessageStep.builder()
                .name("first message name")
                .text("text")
                .build();

        WebsiteStep websiteStep1 = WebsiteStep.builder()
                .name("website step")
                .url("tmax.com")
                .build();

        List<Step> steps = new ArrayList<>();
        steps.add(messageStep1);
        steps.add(websiteStep1);

        return Flow.builder()
                .name("flow1")
                .steps(steps)
                .build();
    }
    public FlowRequestDto makeFlowRequestDto(){
        StepDto messageStep1 = StepDto.builder()
                .id(2L)
                .name("first edited message name")
                .stepType(message)
                .text("edited text")
                .build();
        StepDto websiteStep1 = StepDto.builder()
                .id(3L)
                .name("website step")
                .stepType(website)
                .url("www.superheelo.com")
                .build();
        StepDto messageStep2 = StepDto.builder()
                .name("new messsage")
                .stepType(message)
                .text("new text")
                .build();

        List<StepDto> steps = new ArrayList<>();
        steps.add(messageStep1);
        steps.add(websiteStep1);
        steps.add(messageStep2);

        return FlowRequestDto.builder()
                .id(1L)
                .name("update flow")
                .steps(steps)
                .build();
    }


    protected void updateStep(Flow flow, StepDto stepDto){
        if(stepDto.getStepType()==message)
            updateMessageStep(stepDto);
        else if(stepDto.getStepType()==website)
            updateWebsiteStep(stepDto);
        else
            throw new RuntimeException("존재하지 않는 stepType입니다!");
    }
    private void updateMessageStep(StepDto stepDto){
        MessageStep messageStep = messageStepRepository.findById(stepDto.getId())
                .orElseThrow(() ->
                        new NullPointerException("존재하지 않는 stepId 입니다!")
                );
        messageStep.updateMessageStep(stepDto.getName(), stepDto.getText());
        messageStepRepository.save(messageStep);
    }
    private void updateWebsiteStep(StepDto stepDto){
        WebsiteStep websiteStep = websiteStepRepository.findById(stepDto.getId())
                .orElseThrow(() ->
                        new NullPointerException("존재하지 않는 stepId 입니다!")
                );
        websiteStep.updateWebsiteStep(stepDto.getName(), stepDto.getUrl());
        websiteStepRepository.save(websiteStep);
    }

    protected void createStep(Flow flow, StepDto stepDto){
        if(stepDto.getStepType()==message)
            createMessageStep(flow,stepDto);
        else if(stepDto.getStepType()==website)
            createWebsiteStep(flow,stepDto);
        else
            throw new RuntimeException("존재하지 않는 stepType입니다!");
    }
    private void createMessageStep(Flow flow, StepDto stepDto){
        MessageStep messageStep = stepDto.toMessageStep(flow);
        messageStep.setFlow(flow);
        messageStepRepository.save(messageStep);
    }
    private void createWebsiteStep(Flow flow, StepDto stepDto){
        WebsiteStep websiteStep = stepDto.toWebsiteStep(flow);
        websiteStep.setFlow(flow);
        websiteStepRepository.save(websiteStep);
    }
}
