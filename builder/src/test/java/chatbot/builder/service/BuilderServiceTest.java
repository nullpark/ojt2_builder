package chatbot.builder.service;

import chatbot.builder.MariadbTest;
import chatbot.builder.domain.dto.FlowRequestDto;
import chatbot.builder.domain.dto.FlowResponseDto;
import chatbot.builder.domain.dto.StepDto;
import chatbot.builder.domain.entity.Flow;
import chatbot.builder.domain.entity.MessageStep;
import chatbot.builder.domain.entity.Step;
import chatbot.builder.domain.entity.WebsiteStep;
import chatbot.builder.repository.FlowRepository;
import chatbot.builder.repository.MessageStepRepository;
import chatbot.builder.repository.WebsiteStepRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.transaction.annotation.Transactional;
import org.testcontainers.containers.MariaDBContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static chatbot.builder.domain.dto.StepType.message;
import static chatbot.builder.domain.dto.StepType.website;

@SpringBootTest
@Transactional
class BuilderServiceTest extends MariadbTest {

    @Autowired
    FlowRepository flowRepository;
    @Autowired
    MessageStepRepository messageStepRepository;
    @Autowired
    WebsiteStepRepository websiteStepRepository;
    @Autowired
    FlowSetup flowSetup;

    @Test
    public void Flow업로드및조회(){
        //given : flow를 생성한다
        Flow flow = flowSetup.makeFlow();

        //when : 생성된 flow를 저장한다
        flowRepository.save(flow);

        //then : 저장된 flow를 불러들이고 내용물을 확인한다
        Flow foundFlow = flowRepository.findById(flow.getId()).orElse(null);

        List<Step> steps ;
        steps = flow.getSteps();
        MessageStep messageStep = (MessageStep) steps.get(0);
        WebsiteStep websiteStep = (WebsiteStep) steps.get(1);

        Assertions.assertThat(flow).isEqualTo(foundFlow);
        Assertions.assertThat(messageStep.getName()).isEqualTo("first message name");
        Assertions.assertThat(websiteStep.getName()).isEqualTo("website step");

    }

    @Test
    public void Flow업데이트(){
        //given : 1. flow를 만들고 저장한다. 2. flow를 변화시킬 새로운 flowRequestDto를 만든다
        Flow flow = flowSetup.makeFlow();
        flowRepository.save(flow);
        FlowRequestDto flowRequestDto = flowSetup.makeFlowRequestDto();
        Assertions.assertThat(flow.getId()).isEqualTo(flowRequestDto.getId());

        //when : flowRequestDto를 이용해 이미 저장된 targetFlow를 update + 새로운건 create한다
        Flow targetFlow = flowRepository.findById(flowRequestDto.getId()).
                orElseThrow( () ->
                        new NullPointerException("FlowId 가 존재하지 않습니다!")
                );

        targetFlow.updateName(flowRequestDto.getName());

        for(StepDto stepDto : flowRequestDto.getSteps()){
            if(Objects.isNull(stepDto.getId())){
                flowSetup.createStep(targetFlow, stepDto);
            }
            else{
                flowSetup.updateStep(targetFlow,stepDto);
            }
        }
        //then : Flow가 제대로 update 되었는지 확인한다.
        Flow resultFlow = flowRepository.findById(flowRequestDto.getId()).get();
        List<Step> resultSteps = resultFlow.getSteps();

        Assertions.assertThat(resultSteps.size()).isEqualTo(3);

        MessageStep messageStep1 = (MessageStep) resultSteps.get(0);
        WebsiteStep websiteStep1 = (WebsiteStep) resultSteps.get(1);

        Assertions.assertThat(resultFlow.getName()).isEqualTo(flowRequestDto.getName());
        Assertions.assertThat(messageStep1.getName()).isEqualTo("first edited message name");
        Assertions.assertThat(messageStep1.getText()).isEqualTo("edited text");
        Assertions.assertThat(websiteStep1.getUrl()).isEqualTo("www.superheelo.com");
    }


}