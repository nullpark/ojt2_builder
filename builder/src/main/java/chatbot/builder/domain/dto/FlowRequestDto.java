package chatbot.builder.domain.dto;

import chatbot.builder.domain.entity.Step;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FlowRequestDto {
    private Long id;
    private String name;
    private List<StepDto> steps;
}
