package chatbot.builder.domain.dto;

import chatbot.builder.domain.entity.Flow;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FlowResponseDto {

    private Long id;
    private String name;
    private List<StepDto> steps;

    public static FlowResponseDto toDto(Flow flow){
        return FlowResponseDto.builder()
                .id(flow.getId())
                .name(flow.getName())
                .steps(flow.getSteps().stream().map(StepDto::toDto).collect(Collectors.toList()))
                .build();
    }

}
