package chatbot.builder.domain.dto;

import chatbot.builder.domain.entity.Flow;
import chatbot.builder.domain.entity.MessageStep;
import chatbot.builder.domain.entity.Step;
import chatbot.builder.domain.entity.WebsiteStep;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class StepDto {
    private Long id;

    private String name;
    private StepType stepType;

    //message
    private String text;
    //website
    private String url;


    public MessageStep toMessageStep(Flow flow){
        return MessageStep.builder()
                .flow(flow)
                .text(text == null ? "":text)
                .name(name)
                .build();
    }

    public WebsiteStep toWebsiteStep(Flow flow){
        return WebsiteStep.builder()
                .flow(flow)
                .url(url == null ? "": url)
                .name(name)
                .build();
    }

    public static StepDto toDto(Step step){
        if(step instanceof MessageStep messageStep){
            return toMessageStepDto(messageStep);
        }
        else if (step instanceof WebsiteStep websiteStep) {
            return toWebsiteStepDto(websiteStep);
        }
        return new StepDto();
    }

    public static StepDto toMessageStepDto(MessageStep messageStep){
        return StepDto.builder()
                .id(messageStep.getId())
                .name(messageStep.getName())
                .text(messageStep.getText())
                .stepType(StepType.message)
                .build();
    }

    public static StepDto toWebsiteStepDto(WebsiteStep websiteStep){
        return StepDto.builder()
                .id(websiteStep.getId())
                .name(websiteStep.getName())
                .url(websiteStep.getUrl())
                .stepType(StepType.website)
                .build();
    }
}
