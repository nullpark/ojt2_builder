package chatbot.builder.domain.dto;

public enum StepType {
    message, website
}
