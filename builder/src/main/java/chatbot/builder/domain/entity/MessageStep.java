package chatbot.builder.domain.entity;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@DiscriminatorValue("message")
@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MessageStep extends Step{

    private String text;

    public void setFlow(Flow flow){
        super.setFlow(flow);
    }

    @Builder
    public MessageStep(Flow flow, String name, String text) {
        super(flow,name);
        this.text = text;
    }

    public void updateMessageStep(String name, String text){
        super.updateStep(name);
        this.text = text;
    }
}
