package chatbot.builder.domain.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Where(clause = "deleted=false")
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Flow extends BaseEntity{
    @Id
    @GeneratedValue
    private Long id;
    private String name;

    @JsonManagedReference
    @OneToMany(mappedBy = "flow", cascade= CascadeType.ALL)
    private List<Step> steps = new ArrayList<>();

    @Builder
    public Flow(String name, List<Step> steps){
        this.name = name;
        this.steps = steps;
    }

    public void updateName(String name){
        this.name = name;
    }
}
