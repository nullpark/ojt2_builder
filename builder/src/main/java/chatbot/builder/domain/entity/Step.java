package chatbot.builder.domain.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.sun.istack.NotNull;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Where;

import javax.persistence.*;

@Entity
@Where(clause = "deleted=false")
@Getter
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name="stepType")
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Step extends BaseEntity {
    @Id
    @GeneratedValue
    protected Long id;
    protected String name;

    @JsonBackReference
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "flow_id")
    @NotNull
    private Flow flow;

    protected Step(Flow flow, String name){
        this.flow = flow;
        this.name = name;
    }

    public void setFlow(Flow flow){
        this.flow = flow;
        flow.getSteps().add(this);
    }

    public void updateStep(String name){
        this.name = name;
    }

}
