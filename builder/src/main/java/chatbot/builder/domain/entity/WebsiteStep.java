package chatbot.builder.domain.entity;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@DiscriminatorValue("website")
@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class WebsiteStep extends Step{

    private String url;

    public void setFlow(Flow flow){
        super.setFlow(flow);
    }

    @Builder
    public WebsiteStep(Flow flow, String name, String url) {
        super(flow, name);
        this.url = url;
    }

    public void updateWebsiteStep(String name, String url){
        super.updateStep(name);
        this.url = url;
    }
}
