package chatbot.builder.controller;

import chatbot.builder.domain.dto.FlowRequestDto;
import chatbot.builder.domain.dto.FlowResponseDto;
import chatbot.builder.domain.dto.StepDto;
import chatbot.builder.domain.entity.Flow;
import chatbot.builder.service.BuilderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@RestController
@RequestMapping("/flow")
public class FlowController {

    private final BuilderService builderService;

    @Autowired
    public FlowController(BuilderService builderService){
        this.builderService = builderService;
    }

    @PostMapping
    public ResponseEntity<Void> insertFlow(@RequestBody FlowRequestDto flowRequestDto){
        builderService.createFlow(flowRequestDto);
        return ResponseEntity.ok().build();
    }

    @GetMapping(path="{flowId}")
    public ResponseEntity<FlowResponseDto> getFlow(@PathVariable Long flowId, HttpServletRequest request){
        return ResponseEntity.ok(builderService.findFlow(flowId));
    }

    @PostMapping(path="{flowId}")
    public ResponseEntity<Void> editFlow(@PathVariable Long flowId, @RequestBody FlowRequestDto flowRequestDto){
        builderService.updateFlow(flowRequestDto);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping(path="{flowId}")
    public ResponseEntity<Void> deleteFlow(@PathVariable Long flowId){
        builderService.deleteFlow(flowId);
        return ResponseEntity.ok().build();
    }

    @ExceptionHandler(NoSuchElementException.class)
    public ResponseEntity<Map<String,Object>> handleNoSuchElementFoundException(NoSuchElementException exception){
        Map<String, Object> errorResult = new HashMap<>();
        errorResult.put("status", 400);
        errorResult.put("error message", exception.getMessage());
        return ResponseEntity.status(400).body(errorResult);
    }
}
