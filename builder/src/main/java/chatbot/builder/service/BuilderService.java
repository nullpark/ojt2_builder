package chatbot.builder.service;

import chatbot.builder.domain.dto.FlowRequestDto;
import chatbot.builder.domain.dto.FlowResponseDto;
import chatbot.builder.domain.dto.StepDto;
import chatbot.builder.domain.entity.Flow;
import chatbot.builder.domain.entity.MessageStep;
import chatbot.builder.domain.entity.Step;
import chatbot.builder.domain.entity.WebsiteStep;
import chatbot.builder.repository.FlowRepository;
import chatbot.builder.repository.MessageStepRepository;
import chatbot.builder.repository.WebsiteStepRepository;
import com.sun.jdi.InvalidTypeException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.*;

import static chatbot.builder.domain.dto.StepType.message;
import static chatbot.builder.domain.dto.StepType.website;

@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class BuilderService {
    private final FlowRepository flowRepository;
    private final MessageStepRepository messageStepRepository;
    private final WebsiteStepRepository websiteStepRepository;


    public FlowResponseDto findFlow(Long flowId){
        Flow flow = flowRepository.findById(flowId)
                .orElseThrow(() ->
                        new NoSuchElementException("존재하지 않는 flowId 입니다!")
                );
        return FlowResponseDto.toDto(flow);
    }


    @Transactional
    public void createFlow(FlowRequestDto flowRequestDto) {
        Flow flow = Flow.builder()
                .name(flowRequestDto.getName())
                .steps(new ArrayList<Step>())
                .build();
        flowRepository.save(flow);

        for (StepDto stepDto: flowRequestDto.getSteps()){
            if(Objects.isNull(stepDto.getId()))
                createStep(flow,stepDto);
        }
    }

    @Transactional
    public void updateFlow(FlowRequestDto flowRequestDto){
        Flow targetFlow = flowRepository.findById(flowRequestDto.getId()).
                orElseThrow( () ->
                        new NoSuchElementException("존재하지 않는 flowId 입니다!")
                );

        targetFlow.updateName(flowRequestDto.getName());

        for(StepDto stepDto : flowRequestDto.getSteps()){
            if(Objects.isNull(stepDto.getId())){
                createStep(targetFlow, stepDto);
            }
            else{
                updateStep(targetFlow,stepDto);
            }
        }

    }
    @Transactional
    public void deleteFlow(Long flowId){
        flowRepository.deleteById(flowId);
    }

    private void createStep(Flow flow, StepDto stepDto){
        if(stepDto.getStepType()==message)
            createMessageStep(flow,stepDto);
        else if(stepDto.getStepType()==website)
            createWebsiteStep(flow,stepDto);
        else
            throw new NoSuchElementException("존재하지 않는 stepType입니다!");
    }
    private void createMessageStep(Flow flow, StepDto stepDto){
        MessageStep messageStep = stepDto.toMessageStep(flow);
        messageStep.setFlow(flow);
        messageStepRepository.save(messageStep);
    }
    private void createWebsiteStep(Flow flow, StepDto stepDto){
        WebsiteStep websiteStep = stepDto.toWebsiteStep(flow);
        websiteStep.setFlow(flow);
        websiteStepRepository.save(websiteStep);
    }

    private void updateStep(Flow flow, StepDto stepDto){
        if(stepDto.getStepType()==message)
            updateMessageStep(stepDto);
        else if(stepDto.getStepType()==website)
            updateWebsiteStep(stepDto);
        else
            throw new NoSuchElementException("존재하지 않는 stepType입니다!");
    }
    private void updateMessageStep(StepDto stepDto){
        MessageStep messageStep = messageStepRepository.findById(stepDto.getId())
                .orElseThrow(() ->
                        new NoSuchElementException("존재하지 않는 stepId 입니다!")
                );
        messageStep.updateMessageStep(stepDto.getName(), stepDto.getText());
        messageStepRepository.save(messageStep);
    }
    private void updateWebsiteStep(StepDto stepDto){
        WebsiteStep websiteStep = websiteStepRepository.findById(stepDto.getId())
                .orElseThrow(() ->
                        new NoSuchElementException("존재하지 않는 stepId 입니다!")
                );
        websiteStep.updateWebsiteStep(stepDto.getName(), stepDto.getUrl());
        websiteStepRepository.save(websiteStep);
    }

}
