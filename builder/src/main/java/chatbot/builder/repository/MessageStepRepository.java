package chatbot.builder.repository;

import chatbot.builder.domain.entity.MessageStep;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MessageStepRepository extends JpaRepository<MessageStep,Long> {
}
