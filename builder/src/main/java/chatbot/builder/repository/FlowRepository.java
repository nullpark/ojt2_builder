package chatbot.builder.repository;

import chatbot.builder.domain.entity.Flow;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FlowRepository extends JpaRepository<Flow, Long> {
}