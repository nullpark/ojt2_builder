package chatbot.builder.repository;

import chatbot.builder.domain.entity.WebsiteStep;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WebsiteStepRepository extends JpaRepository<WebsiteStep,Long> {
}
